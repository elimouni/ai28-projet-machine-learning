# Ai28 Projet Machine Learning sur Adults Income.

Repository du projet de Machine Learning pour l'UV AI28, utilisant le dataset Adults Income.

## Comment utiliser le Jupyter Notebook

Dans le dossier de base du dépôt, ajouter un dossier _data_ contenant le fichier _adults.csv_. Executer ensuite Jupyter pour ouvrir le Notebook. Il faut veiller à ne pas l'uploader (le fichier _.gitignore_ doit ignorer ce dossier) afin de ne pas surcharger le dépôt.
